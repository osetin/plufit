��    V      �     |      x     y  M   �     �  ,   �          4     M     f  0   �  %   �     �  C   �     +	      H	     i	  [   r	     �	  !   �	     �	     
     
     
      
     -
     2
     7
     D
     P
     X
     d
     �
     �
     �
     �
     �
  (   �
     �
               ,     @     P     f     v  	   |     �     �  ?   �  2   �                     >     U     a  
   {     �  	   �     �  
   �  &   �  )   �     "  |   /     �     �  #   �  R     "   Y     |     �  !   �     �  	   �     �  n   �     A  	   [  	   e     o     |     �     �  	   �     �  �  �     �  �   �  (   9  Y   b  8   �  4   �  9   *  5   d  O   �  -   �       R   4  I   �  9   �       �        �  =   �     �               )     C  
   W     b     z     �     �  #   �  J   �          )     A     [     {  �   �  .   =  $   l  '   �  '   �  -   �  6     #   F     j     z     �  %   �  _   �  |   $     �     �      �      �       Z     #   v  Y   �  :   �  ^   /     �  �   �  &   t  !   �  �   �  (   x  ?   �  I   �  �   +  I   �     �        3         A   	   ^      h   �   u       &!     G!  	   Y!     c!     p!     u!     {!  	   �!     �!     *   !   :                  =   U          F       %   T      8   >       '   &                 J   $       ,           2           .                  Q      3           "       O       K                (   4   G       E   1               S      P   
                    L   	   A                B   )              @   M   /       H       #      R      5           6   9   <              7   -   +   I                 C          ?             D          V   ;          N   0    <b>API ID:</b> <b>Like buttons</b> output <b>ONLY</b> bottom page and <b>ONLY</b> in column! <b>Required Field</b> But will have to sort <b>ALL</b> the buttons Button horizontal position Button vertical position Button with mini counter Button with textable counter Choose the display style for your social buttons Class Share Buttons already declared! Color scheme Determines the size and amount of social context next to the button Enable/Disable Share buttons Exclude pages and posts with IDs Facebook Files must be <b>.jpg, .gif, .png</b> extension, the desired size of <b>100x100 pixels</b>. Google-Buzz Header text before social buttons Height Home In My World Interesting Layout style Left Like Like Buttons LiveJournal Mail.ru Mini button Mini button and counter bottom Odnoklassniki On bottom of post On top of post Other Settings Pages Please drag and drop buttons for sorting Position Share Buttons Post to Facebook Post to Google Buzz Post to LiveJournal Post to MyWorld Post to Odnoklassniki Post to Twitter Posts Recommend Right Save Changes Select which side you want to display the button: right or left Sets up before or after post/page button are shown Share Share Buttons Share Buttons Plugin Settings Share Buttons Settings Share to FB Show Facebook Like Button Show Faces Show Mailru Like Button Show Text Show Vkontakte.ru Like Button Show faces Show profile pictures below the button Sorry!  There are no items in the system. Sort buttons Specify IDs of pages and posts which should stay without buttons (separated by commas, eg <code>4, 8, 15, 16, 23, 42</code>) The Button displays on The color scheme of the plugin The height of the plugin, in pixels The verb to display in the button. Currently only Like and Recommend are supported The width of the plugin, in pixels Twitter Twitter via Upload picture for your site-logo Verb to display Vkontakte Width You can get your <b>"api_id"</b> on this <b><a href="http://vkontakte.ru/apps.php?act=add&site=1">link</a></b> Your Nickname without "@" Your text box_count button_count dark light like recommend standart Project-Id-Version: Share Buttons
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2011-01-20 19:40+0600
PO-Revision-Date: 
Last-Translator: Loskutnikov Artem <artlosk@gmail.com>
Language-Team: artlosk Project <artlosk@gmail.com>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-Language: Russian
X-Poedit-Country: KYRGYZSTAN
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: _e;__
X-Poedit-Basepath: D:\share-buttons
X-Poedit-SearchPath-0: D:\share-buttons
 <b>API ID:</b> <b>Кнопки Нравится</b> выводятся <b>ТОЛЬКО</b> внизу страницы и <b>ТОЛЬКО</b> в столбик! <b>Обязательное поле</b> Но сортировать придётся всё равно <b>ВСЕ</b> кнопки Горизонтальная позиция кнопок Вертикальная позиция кнопок Кнопка с миниатюрным счетчиком Кнопка с текстовым счетчиком Выберете стиль, который будет отображаться Класс Share Buttons уже вызван! Цветовая схема Определите какую кнопку хотите использовать Включение/Отключение социальных кнопок Исключить страницы и посты по ID Facebook Файлы должны быть с расширением <b>.jpg, .gif, .png</b>, желательный размер <b>100x100 пикселей</b>. Google-Buzz Текст перед социальными кнопками Высота На главную В Мой Мир Это интересно Тип кнопки Слева Мне нравится Кнопки Нравится LiveJournal Mail.ru Миниатюрная кнопка Миниатюрная кнопка со счетчиком наверху Odnoklassniki Внизу записи Вверху записи Другие настройки На страницах Чтобы отсортировать кнопки, хажмите левой кнопкой мыши картинку и перетащите в нужное место Позиция кнопок соц. сетей Опубликовать в Facebook Опубликовать в Google-Buzz Опубликовать в LiveJournal Опубликовать в Моём Мире Опубликовать в Одноклассники Опубликовать в Twitter В постах Рекоммендую Справа Сохранить изменения Выберете где отображаться кнопкам: слева или справа Установите где отображаться кнопкам перед записью или после записи Поделиться Share Buttons Настройки Share Buttons Настройки Share Buttons Поделиться Показывать кнопку Мне нравится для соц. сети Facebook Показывать профили Показывать кнопку Мне нравится для соц. сети Mail.ru Показывать текст рекоммендации Показывать кнопку Мне нравится для соц. сети Vkontakte.ru Показывать фото Показывать изображения профилей, которые нажали на кнопку (не забудьте поменять высоту, примерно 70 пикселей) Извините! Нет данных. Сортировка кнопок Вы можете исключить записи и страницы, используя их ID, впишите ID разделяя запятыми (<code>4, 8, 15, 16, 23, 42</code>) Где показывать кнопки Цветовая схема: светлая или темная Высота контейнера (НЕ КНОПКИ) в пикселах Слово, которое будет отображаться на кнопке: Мне нравится и Рекоммендую Ширина контейнера (НЕ КНОПКИ) в пикселах Twitter Twitter via Загрузите ваш логотип сайта Слово на кнопке Vkontakte Ширина Вы можете получить ваш <b>"api_id"</b>, если передете по этой <b><a href="http://vkontakte.ru/apps.php?act=add&site=1">ссылке</a></b> Ваш никнейм без "@" Ваш текст box_count button_count dark light like recommend standart 